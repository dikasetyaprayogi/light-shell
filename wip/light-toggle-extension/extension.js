/* extension.js
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-2.0-or-later
 */

//// summary:
//// extension.js - provide quick settings toggle button
//// stylesheet-dark.css & stylesheet-light.css - override GNOME's prefer-light theme with custom "fully light" prefer-light theme

//// some parts referenced from official GNOME shell UI DarkMode.js

//// todo: create override function to remove GNOME's dark style toggle button and let this extension button take the role as full dark / full light toggle

import Gio from 'gi://Gio';
import GObject from 'gi://GObject';

import * as Main from 'resource:///org/gnome/shell/ui/main.js';

import {Extension, gettext as _} from 'resource:///org/gnome/shell/extensions/extension.js';
import {QuickToggle, SystemIndicator} from 'resource:///org/gnome/shell/ui/quickSettings.js';

//// main function
const LightModeToggle = GObject.registerClass(
class LightModeToggle extends QuickToggle {
    _init() {
        super._init({
            title: _('Light Style'),
            iconName: 'weather-clear-symbolic',
        });

        this._settings = new Gio.Settings({
            schema_id: 'org.gnome.desktop.interface',
        });
        this._changedId = this._settings.connect('changed::color-scheme',
            () => this._sync());

        this.connectObject(
            'destroy', () => this._settings.run_dispose(),
            'clicked', () => this._toggleMode(),
            this);
        this._sync();
    }

    _toggleMode() {
        Main.layoutManager.screenTransition.run();
        this._settings.set_string('color-scheme',
            this.checked ? 'default' : 'prefer-light');
    }

    _sync() {
        const colorScheme = this._settings.get_string('color-scheme');
        const checked = colorScheme === 'prefer-light';
        if (this.checked !== checked)
            this.set({checked});
    }
});

//// push toggle
const ExampleIndicator = GObject.registerClass(
class ExampleIndicator extends SystemIndicator {
    _init() {
        super._init();

        this.quickSettingsItems.push(new LightModeToggle());
    }
});

export default class QuickSettingsExampleExtension extends Extension {
    enable() {
        //// add all things
        this._init = new ExampleIndicator();
        Main.panel.statusArea.quickSettings.addExternalIndicator(this._init);
    }

    disable() {
        //// remove all things
        this._init.quickSettingsItems.forEach(item => item.destroy());
        this._init.destroy();
        
        //// reset color-scheme to "default"
        this._settings = new Gio.Settings({
            schema_id: 'org.gnome.desktop.interface',
        });
            
        Main.layoutManager.screenTransition.run();
        this._settings.set_string('color-scheme',
             'default');
        //// todo: automation: if the last colorscheme isn't light, get and restore the last colorscheme (prefer-dark or default)
        //// this avoid the theme gets changed in case user still in dark mode and this extension is disabled
    }
}
