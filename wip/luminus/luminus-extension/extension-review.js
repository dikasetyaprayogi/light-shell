//                    REVIEWS
//
// 1. Create `destroy()` method in that class instead
// 2. You cannot use run_dispose in extensions
// 3. use less generic class name
// 4. should be null out in disable
// 5. not a good idea to enable and disable again
// 6. use a better property name. `_init` has a meaning and that's not related to that instance
// 7. wrong usage of `disconnectObject()`

import Gio from 'gi://Gio';
import GObject from 'gi://GObject';

import * as Main from 'resource:///org/gnome/shell/ui/main.js';

import {Extension, gettext as _} from 'resource:///org/gnome/shell/extensions/extension.js';
import {QuickToggle, SystemIndicator} from 'resource:///org/gnome/shell/ui/quickSettings.js';

// toggle button //
const LightModeToggle = GObject.registerClass(
class LightModeToggle extends QuickToggle {
    _init() {
        super._init({
            title: _('Light Style'),
            iconName: 'weather-clear-symbolic',
        });

        this._settings = new Gio.Settings({
            schema_id: 'org.gnome.desktop.interface',
        });
        this._changedId = this._settings.connect('changed::color-scheme',
            () => this._sync());
            
        // 1. Create `destroy()` method in that class instead
        //// added _destroy() for disconnect signal and null out
        this.connectObject(
            'destroy', () => this._destroy(),
            'clicked', () => this._toggleMode(),
            this);
        this._sync();
    }

    _destroy() {
      // 2. You cannot use run_dispose in extensions
      //// removed, use null
      this._settings = null;
      this._settings.disconnect(this._changedId);
      this.disconnectObject(this);
    }

    _toggleMode() {
        Main.layoutManager.screenTransition.run();
        this._settings.set_string('color-scheme',
            this.checked ? 'default' : 'prefer-light');
    }

    _sync() {
        const colorScheme = this._settings.get_string('color-scheme');
        const checked = colorScheme === 'prefer-light';
        if (this.checked !== checked)
            this.set({checked});
    }    
});

// 3. use less generic class name
//// ExampleIndicator -> LuminusIndicator

// push toggle //
const LuminusIndicator = GObject.registerClass(
class LuminusIndicator extends SystemIndicator {
    _init() {
        super._init();

        this.quickSettingsItems.push(new LightModeToggle());
    }
});

export default class LuminusExtension extends Extension {

    // sync state //
    
    // adapts when extension enabled / user login
    syncEnable() {        
        const colorScheme = this._settings.get_string('color-scheme');
        const isDark = colorScheme === 'prefer-dark';
        if(isDark) {
            Main.layoutManager.screenTransition.run();
            this._settings.set_string('color-scheme', 'prefer-dark');
        } else {
            Main.layoutManager.screenTransition.run();
            this._settings.set_string('color-scheme', 'prefer-light');
        };
    }

    // adapts when extension disabled, user logout
    syncDisable() {        
        const colorScheme = this._settings.get_string('color-scheme');
        const isDark = colorScheme === 'prefer-dark';
        if(isDark){
            Main.layoutManager.screenTransition.run();
            this._settings.set_string('color-scheme', 'prefer-dark');
        } else {
            Main.layoutManager.screenTransition.run();
            this._settings.set_string('color-scheme', 'default');
        };
    } 

    enable() {   
        // 4. (this._settings = ) should be null out in disable
        //// added in disable
    
        // adapts colorscheme
        this._settings = new Gio.Settings({schema_id: 'org.gnome.desktop.interface',});    
        this.syncEnable();
        
        // 6. use a better property name. `_init` has a meaning and that's not related to that instance
        //// changed _init -> _indicator
        
        // push
        this._indicator = new LuminusIndicator();
        Main.panel.statusArea.quickSettings.addExternalIndicator(this._indicator);
    }
    
    disable() {
        // adapts colorscheme
        this.syncDisable();
        this._settings = null;

        // clean
        this._indicator.quickSettingsItems.forEach(item => item.destroy());
        this._indicator.destroy();
        this._indicator = null;
        
        // 5. not a good idea to enable and disable again
        // removed from disable - wrong method syncEnable(null)
        
        // 7. wrong usage of `disconnectObject()`
        //// removed from diable - added disconnectObject(this) in _destroy() of class LightModeToggle
    }  
}
