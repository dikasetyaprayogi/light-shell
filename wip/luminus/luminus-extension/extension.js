/*{
                           LUMINUS
                    gnome-shell extension
                 
A simple extension that loads custom full-light stylesheet and
adds "Light Style" toggle button into the quick settings panel.

             based on gnome-shell's darkMode.js

https://gitlab.com/dikasetyaprayogi/luminus-gnome-shell-extension



This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

SPDX-License-Identifier: GPL-2.0-or-later
}*/

import Gio from 'gi://Gio';
import GObject from 'gi://GObject';

import * as Main from 'resource:///org/gnome/shell/ui/main.js';

import {Extension, gettext as _} from 'resource:///org/gnome/shell/extensions/extension.js';
import {QuickToggle, SystemIndicator} from 'resource:///org/gnome/shell/ui/quickSettings.js';

// toggle button
const LightModeToggle = GObject.registerClass(
class LightModeToggle extends QuickToggle {
    _init() {
        super._init({
            title: _('Light Style'),
            iconName: 'weather-clear-symbolic',
        });

        this._settings = new Gio.Settings({
            schema_id: 'org.gnome.desktop.interface',
        });
        this._changedId = this._settings.connect('changed::color-scheme',
            () => this._sync());

        this.connectObject(
            'destroy', () => {
            this._settings.run_dispose();
            this._settings.disconnect(this._changedId);
            },
            'clicked', () => this._toggleMode(),
            this);
        this._sync();
    }

    _toggleMode() {
        Main.layoutManager.screenTransition.run();
        this._settings.set_string('color-scheme',
            this.checked ? 'default' : 'prefer-light');
    }

    _sync() {
        const colorScheme = this._settings.get_string('color-scheme');
        const checked = colorScheme === 'prefer-light';
        if (this.checked !== checked)
            this.set({checked});
    }
    
});

// push toggle
const ExampleIndicator = GObject.registerClass(
class ExampleIndicator extends SystemIndicator {
    _init() {
        super._init();

        this.quickSettingsItems.push(new LightModeToggle());
    }
});

export default class LuminusExtension extends Extension {

    // adapts when extension enabled / user login
    _syncEnable() {
        this._settings = new Gio.Settings({
        schema_id: 'org.gnome.desktop.interface',
        });
        
        const colorScheme = this._settings.get_string('color-scheme');
        const isDark = colorScheme === 'prefer-dark';
        if(isDark) {
            Main.layoutManager.screenTransition.run();
            this._settings.set_string('color-scheme', 'prefer-dark');
        } else {
            Main.layoutManager.screenTransition.run();
            this._settings.set_string('color-scheme', 'prefer-light');
          };
        }

    // adapts when extension disabled, user logout
    _syncDisable() {
        this._settings = new Gio.Settings({
        schema_id: 'org.gnome.desktop.interface',
        });
        
        const colorScheme = this._settings.get_string('color-scheme');
        const isDark = colorScheme === 'prefer-dark';
        if(isDark){
            Main.layoutManager.screenTransition.run();
            this._settings.set_string('color-scheme', 'prefer-dark');
        } else {
        Main.layoutManager.screenTransition.run();
        this._settings.set_string('color-scheme', 'default');
          };
        }

    enable() {
        this._syncEnable();
        
        // preps
        this._init = new ExampleIndicator();
        Main.panel.statusArea.quickSettings.addExternalIndicator(this._init);
    }
    
    disable() {        
        this._syncDisable();

        // cleanup
        this._init.quickSettingsItems.forEach(item => item.destroy());
        this._init.destroy(); // run dispose and disconnect signal
        this._init.disconnectObject();    
        this._init = null;
        
        this._syncEnable(null);
        this._syncDisable(null);
    }
}

/*{
TODO: full integration with GNOME desktop a.k.a replace the 'default' theme completely when extension is enabled

Luminus goal is to bring full light theme experience. this means both the gnome-shell (through stylesheet css)
and the GTK apps (through colorscheme key). currently we achieve this through adding yet another "Light Style"
button into quick settings panel. it's sufficient but would be nice if we can act as sole light theme when enabled.

the main problem is, as long GNOME's stock "Dark Style" toggle button exposed in quick settings panel it carry out
possible redirection of 'prefer-dark'<->'default' that can overwrite and lock out prefer-light colorscheme (GTK apps).
loupe/image viewer app is an easy indicator of GTK colorscheme state: dark=default/prefer-dark light=prefer-light.

possible solution:
- block/remove GNOME's stock "Dark Style" toggle and adds our own button with 'prefer-dark':'prefer-light' toggle
- override/bind the 'default' theme into 'prefer-light' ("both" for the gnome-shell style & GTK apps colorscheme).
}*/

