<div align="center">

:warning: later Light Shell release will be merged and continue within [Luminus Project](https://gitlab.com/dikasetyaprayogi/luminus-project) that brings new vision, various improvement and better integrations :warning:

### THE LIGHT SHELL PROJECT

an alternative full light style theme for GNOME Shell

<img width="683" src="/extra/banner.png">

[download wallpaper](https://gitlab.com/dikasetyaprayogi/light-shell/-/raw/main/release/wallpaper-day.png?ref_type=heads&inline=false)

<img src="/extra/spacer.png">

</div>

### About

Light Shell is a distribution of GNOME Shell theme through extension that aim for easy installation and upgrade. by itself, it's a minimal tweaks around GNOME Shell stylesheet that provide fully light theme variant.

Light Shell started as community effort for the needs of light aesthetic fans because the stock GNOME Shell doesn't provide the light theme experience that most of us expect it to be [(see GNOME gitlab discussion)](https://gitlab.gnome.org/GNOME/gnome-shell/-/issues/6902#note_1945512)

### Installation

simply browse and install from [extensions manager](https://flathub.org/apps/com.mattjakeman.ExtensionManager) or [gnome shell extension website](https://extensions.gnome.org/extension/6102/light-shell/)

if it doesns't work for you, try [manual installation](https://gitlab.com/dikasetyaprayogi/light-shell/-/tree/main/release?ref_type=heads)

### Build

clone this repo (or the build folder only) then invoke "sassc" inside it
```
$ sassc ./build/light-shell/theme/gnome-shell-light.scss > gnome-shell.css
```
more info and hacking guide [here](https://gitlab.com/dikasetyaprayogi/light-shell/-/tree/main/build?ref_type=heads)

### Tips

**quick theme switching**: you can use [extension list](https://extensions.gnome.org/extension/3088/extension-list/) to turn Light Shell extension on/off easily

**consistent theme for older apps**: see [adw-gtk3 project](https://github.com/lassekongo83/adw-gtk3) to apply uniform light theme to old GTK style apps

**override default dark apps**: useful for apps that doesn't provide theme setting (libadwaita only)
```
$ gsettings set org.gnome.desktop.interface color-scheme prefer-light
```
to revert, change "prefer-light" to "default"

for further tips & tricks see [here](https://gitlab.com/dikasetyaprayogi/light-shell/-/tree/main/extra?ref_type=heads)

### Screenshots

a quick tour of current Light Shell release

<img width="683" src="/extra/screenshots/ss1.png">
<img width="683" src="/extra/screenshots/ss2.png">
<img width="683" src="/extra/screenshots/ss3.png">
<img width="683" src="/extra/screenshots/ss4.png">
<img width="683" src="/extra/screenshots/ss5.png">
<img width="683" src="/extra/screenshots/ss6.png">
<img width="683" src="/extra/screenshots/ss7.png">

### Attribution

- the project that inspired Light Shell: https://gitlab.gnome.org/eeshugerman/gnome-shell-theme-default-light

- official GNOME light style: https://extensions.gnome.org/extension/6198/light-style/

- the GNOME project: https://www.gnome.org/
