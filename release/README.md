## Light Shell Release

### Files

*gnome-shell.css*

the ready to use stylesheet generated from build folder. make sure to use Light Shell version that close or match with your current GNOME version.

*wallpaper-day.png & wallpaper-night.png*

Light Shell ship with its own signature [solar blue wallpaper series](https://gitlab.com/dikasetyaprayogi/light-shell/-/blob/main/release/wallpaper-day.png?ref_type=heads) + [extra night wallpaper](https://gitlab.com/dikasetyaprayogi/light-shell/-/blob/main/release/wallpaper-night.png?ref_type=heads) for every release (since 45+) you can download it directly from gitlab.

### Manual Installation

in case Light Shell extension doesn't work try to install it as user themes

1. download the [gnome-shell.css](https://gitlab.com/dikasetyaprayogi/light-shell/-/raw/main/release/gnome-shell.css?ref_type=heads&inline=false)
```
$ mkdir ~/.themes/light-shell/gnome-shell
$ cp gnome-shell.css ~/.themes/light-shell/gnome-shell/gnome-shell.css 
```
1. install and enable [user themes extension](https://extensions.gnome.org/extension/19/user-themes/)
1. from its setting choose light-shell

