## Light Shell Build

### Files

*light-shell folder*

synced from `./data/theme` part from official [gnome shell gitlab](https://gitlab.gnome.org/GNOME/gnome-shell/-/tree/main/data/theme?ref_type=heads). these are the only needed files.

*gnome-shell-snapshot.zip*

the backup of GNOME Shell version which is Light Shell currently made of.

### Development

Releases are worked out when there is new GNOME shell version release. if there a need for important bug fixes, a release point will be made as well. Light Shell version follow the GNOME Shell version snapshot.

the purpose of this project is only to made full light variant of GNOME Shell, not to made a brand new theme so custom things like transparency, blurs, accent color etc.. is out of scope.

### Building Light Shell

Building process consist of modifying the scss file, generate it with sassc, put in .themes folder and test load the theme repeatedly until desired result achieved.

1. install "sassc" on your system (the name might differ depending on your linux distro)
2. clone this repo and invoke sassc on build folder. example:
```
$ git clone https://gitlab.com/dikasetyaprayogi/light-shell
$ cd ./light-shell/build/light-shell/theme/
$ sassc gnome-shell-light.scss > gnome-shell.css
```
the result is a ready to use Light Shell stylesheet.

### Building Light Shell Extension

Basically just a empty default extension to load custom stylesheet. gnome-shell.css generated builds are renamed into stylesheet.css and put in there. all light shell extension zip archive can be downloaded in gnome extension website.

### Hacking

Light Shell changes are marked as `//lsc ` inside css file lines of code. you can use diff program to compare the theme folder from *gnome-shell-snapshot* and *light-shell* folder to list all the diffs.

these saas folder is the place you might probably interested:
```
_pallete.scss - the base color definition, easily swap or assign your color here
_colors.scss - global color reference, start your journey here
_common.scss - commmon styling you probably wont touch but worth to inspect
widgets folder - special style for each separated component
```
look into GNOME Shell readme for useful info as well.

### Guidelines

light variation simulation can be done by taking a screenshot of GNOME session in dark mode then use image editor to invert and see how overall light colors should look like.

the color must be inverted whenever possible, use forced color only when necessary. mostly for pure white/black color that will look weird if inverted.

```
css invert function = invert(colour)

example inversion recommendation

fg color <-> bg color
bg color <-> fg color
$dark_2 <-> light_4
$dark_3 <-> $light_3
$dark_4 <-> $light_2

pure colors inversion recomendation

$dark_5/black <-> $light-3,2,1/white
```
### Example

changing some color:

```
$ git clone https://gitlab.com/dikasetyaprayogi/light-shell
```

edit `./light-shell/build/light-shell/theme/gnome-shell-sass/_palette.scss`

the default accent color reference used in GNOME is `$blue_3: #3584e4;`. replace it with accent color of your choice.

navigate back to theme folder and regenerate the final stylesheet with sassc:
```
$ cd ./light-shell/build/light-shell/theme/
$ sassc gnome-shell-light.scss > gnome-shell.css
```
there you had ready to use gnome-shell.css custom Light Shell with your custom color.

then you can test load it or install as theme (refer to manual installaton tutorial in release folder).
