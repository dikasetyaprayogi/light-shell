## Tips & Tricks

### port Light Shell as user theme
```
$ mkdir ~/.themes/light-shell/gnome-shell
$ cp ~/.local/share/gnome-shell/extensions/lightshell@dikasp.gitlab/stylesheet.css ~/.themes/light-shell/gnome-shell/gnome-shell.css
```
then it should be visible as "light-shell" in user theme extension or gnome tweaks.

### force rounded window corners for older gtk apps

see [rounded window corner extension](https://extensions.gnome.org/extension/5237/rounded-window-corners)

### custom GDM (experimental)

see [gdm-settings project](https://github.com/gdm-settings/gdm-settings)

### using Light Shell alongside other theming extension

Light Shell will clash with other extension that uses default extension stylesheet, a extension that can overrride extension theme is recommended, here a small list of theming extension that can work well with:
1. [Blur my Shell](https://extensions.gnome.org/extension/3193/blur-my-shell/) for desktop eyecandy. needs [Blur my Light Shell version](https://extensions.gnome.org/extension/6121/blur-my-light-shell/)
1. [dash to panel](https://extensions.gnome.org/extension/307/dash-to-dock/) for Windows like appearance. should work out of the box
1. [dash to dock](https://extensions.gnome.org/extension/307/dash-to-dock/) for Mac or Ubuntu like appearance. color adjust available in its settings

### experimenting with Light Shell

you can experimenting with Light Shell with either directly edit the extension stylesheet `~/.local/share/gnome-shell/extensions/lightshell@dikasp.gitlab/stylesheet.css` or via user themes `~/.themes/light-shell/gnome-shell/gnome-shell.css`

these just meant as a quick test/live debug, a global modification better done through full build (see tutorial in build folder). the theme/extension also need to be reloaded to take effect.



